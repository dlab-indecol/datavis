"""
Python 3 example to use API for exporting the map from data_json input.

The API return status and in case of status=success if the map was exported.
The map is created in memory on server and returned as client response. 
"""

import os
import json
from urllib.request import Request, urlopen
from urllib.error import URLError
import ssl
import shutil


url_indecol_server = 'https://datavis.indecol.no/plot/worldmap'

data_json = {
    "data": [
        {"code": "USA", "value": 1.1},
        {"code": "NOR", "value": 2.2},
        {"code": "DEU", "value": 3.3}
    ],
    "configuration": {"version": 0.1,
                      "mapType": "World default",
                      "title": "World Map",
                      "subtitle": "Subtitle",
                      "colors": [[0, "#990041"], [0.1, "#550041"], [0.6, "#000041"], [0.9, "#115541"]],
                      "NaNColor": "#ffffff",
                      "backgroundColor": "#ffffff",
                      "minValue": "1",
                      "maxValue": "100",
                      "scale": "linear",
                      "legendText": "Legend",
                      "unit": "",
                      "showCountryNames": False,
                      "valueFormat": ""},
    "export": {
        "type": 'jpeg',  # optional must be png, jpeg, svg or pdfm (default is png)
        "width": 400,  # optional
        }
    }

params = json.dumps(data_json).encode('utf8')

req = Request(url_indecol_server, data=params,
              headers={'content-type': 'application/json'})
try:
    context = ssl._create_unverified_context()
    response = urlopen(req, context=context)
except URLError as e:
    if hasattr(e, 'reason'):
        print('We failed to reach a server.')
        print('Reason: ', e.reason)
    elif hasattr(e, 'code'):
        print('The server could not fulfill the request.')
        print('Error code: ', e.code)
else:
    # Download the zipped file from `url` and save it locally under `file_name.zip`:
    with open(os.path.join(os.path.curdir, 'graphs.zip'), 'wb') as out_file:
        shutil.copyfileobj(response, out_file)
        print('Map exported to: {}'.format(os.path.join(os.path.curdir, 'graphs.zip')))

