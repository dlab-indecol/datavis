"""
Python 3 example to use API for creating the nested pie from data_json input.

The API return status and in case of status=success the url variable
is provided. The url can be shared and used any time.

The url_... should be used accordingly.
"""

import json
import os
from urllib.request import Request, urlopen
from urllib.error import URLError
import ssl

# specifies the url for creating nestedpie chart
url_indecol_server = 'https://datavis.indecol.no/plot/nestedpie'

# open json example file
with open(os.path.join(os.path.curdir, 'complex_data_example.json')) as json_file:
    data = json.load(json_file)

    params = json.dumps(data).encode('utf8')

    req = Request(url_indecol_server, data=params,
                  headers={'content-type': 'application/json'})
    try:
        context = ssl._create_unverified_context()
        response = urlopen(req, context=context)
    except URLError as e:
        if hasattr(e, 'reason'):
            print('We failed to reach a server.')
            print('Reason: ', e.reason)
        elif hasattr(e, 'code'):
            print('The server could not fulfill the request.')
            print('Error code: ', e.code)
    else:
        res = response.read().decode('utf8')
        status = json.loads(res)['status']
        # the url contains created map and can be opened and shared anytime
        url = json.loads(res)['url']
        print(status, url)
